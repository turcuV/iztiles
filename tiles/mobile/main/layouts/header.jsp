<%-- 
    Document   : header
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>


<base href="${baseURL}/">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>iZSearch | <c:out value="${title}" default="Search it easy!"/></title>
<link rel="shortcut icon" type="image/png" href="${baseURL}/resources/img/favicon.png"/>
<link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/mobile/js/vendors/bootstrap/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/mobile/main.css"/>
<script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/vendors/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/vendors/bootstrap/js/bootstrap.min.js"></script>