<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<c:set var="req" value="${pageContext.request}" />
<c:set var='domain' value='${req.scheme}://${req.serverName}:${req.serverPort}'/>
<c:set var="queryString" value="${req.queryString}"/>
<c:set var="baseURL" value="${domain}${req.contextPath}${queryString}"/>
<c:set var="lang" value="${fn:substring(requestScope.locale,0, 2)}"/>

<!DOCTYPE html>
<html lang="<c:out value="${lang}" default="en"/>" >
	<head>
		<tiles:insertAttribute name="header"/>
	</head>
	<body>
		<div class="wrapper">
			<div class="content">
				<tiles:insertAttribute name="content"/>
			</div>
		</div>
		<tiles:insertAttribute name="footer"/>

		<script>
		$(document).ready(function () {
			$('#social').share({
				networks: ['facebook','twitter'],
				theme: 'square',
				urlToShare: 'http://izsearch.com/about.html'
			});
		});
		</script>
	</body>
</html>
