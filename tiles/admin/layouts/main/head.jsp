<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<meta http-equiv="X-UA-Compatible" content="IE=edge; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="izSearch search engine, the new private way to search the web. Search engine that finds and returns relevant web sites, images, videos and realtime results. Search easy with izSearch!">
<meta name="keywords" content="search engine, web search, privacy, private search, image search, video search, search engine privacy, search engine optimization, search engine marketing, easy search">
<title>iZSearch Admin Dashboard</title>

<link rel="shortcut icon" type="image/png" href="${baseURL}/img/favicon.png"/>

<!-- start: CSS -->
<link id="bootstrap-style" href="${baseURL}/resources/css/admin/bootstrap.min.css" rel="stylesheet">
<link href="${baseURL}/resources/css/admin/bootstrap-responsive.min.css" rel="stylesheet">
<link id="base-style-responsive" href="${baseURL}/resources/css/admin/style-responsive.css" rel="stylesheet">
<link href="${baseURL}/resources/css/admin/jquery.dataTables.min.css" rel="stylesheet">
<link id="base-style" href="${baseURL}/resources/css/admin/style.css" rel="stylesheet">

<link href="${baseURL}/resources/css/admin/main.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
<!-- end: CSS -->

<script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/can.min.js"></script>
<script type="text/javascript" src='${baseURL}/resources/js/production/admin/datatable.min.js'></script>
<script type="text/javascript" src="${baseURL}/resources/js/production/admin/custom.js"></script>

<%--<script src="${baseURL}/resources/js/production/admin/jquery-migrate-1.0.0.min.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery-ui-1.10.0.custom.min.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery.ui.touch-punch.js"></script>
<script src="${baseURL}/resources/js/production/admin/modernizr.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery.cookie.js"></script>
<script src='${baseURL}/resources/js/production/admin/fullcalendar.min.js'></script>
<script src='${baseURL}/resources/js/production/admin/jquery.dataTables.min.js'></script>
<script src="${baseURL}/resources/js/production/admin/excanvas.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery.flot.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery.flot.pie.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery.flot.stack.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery.flot.resize.min.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery.chosen.min.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery.uniform.min.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery.cleditor.min.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery.noty.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery.elfinder.min.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery.raty.min.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery.iphone.toggle.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery.uploadify-3.1.min.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery.gritter.min.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery.imagesloaded.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery.masonry.min.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery.knob.modified.js"></script>
<script src="${baseURL}/resources/js/production/admin/jquery.sparkline.min.js"></script>
<script src="${baseURL}/resources/js/production/admin/counter.js"></script>
<script src="${baseURL}/resources/js/production/admin/retina.js"></script>--%>

<!-- end: JavaScript-->


<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<link id="ie-style" href="css/ie.css" rel="stylesheet">
<![endif]-->

<!--[if IE 9]>
<link id="ie9style" href="css/ie9.css" rel="stylesheet">
<![endif]-->

<!-- start: Favicon -->
<link rel="shortcut icon" href="img/favicon.ico">
<!-- end: Favicon -->


