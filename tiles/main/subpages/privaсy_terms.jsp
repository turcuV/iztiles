<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var='title' value='private search' scope="request"/>

<div class="container-fluid">
	<div id="logo" class="text-center">
		<a href="${baseURL}" title=""><img src="${baseURL}/resources/img/logo.svg" alt="logo"></a>
	</div>
	<div id="motto">
		<div class="text-center">
			<h2>Privacy and Terms</h2>
			<h1 class="grob">Privacy and Terms</h1>
		</div>
	</div>
</div>

<div id="arts">
	<div class="col-md-8">
		<ul class="arts list-unstyled">
			<li>
				<h3>Personal Information</h3>
           <p>iZSearch does not retain or share personal information with any third party, including its provider(s) or sponsored search results.</p>
           <p>Some results page may include third party products or services such a small number of sponsored links to generate revenue and cover operational costs. Those links are retrieved from reputed platforms such as Google AdSense, eBay and others. However, all these links are safe for the user, because they do not transmit any personal information to the site owners. Furthermore, all of these links are processed first by engine iZSearch, which first clear it and after proxy to сorresponding resources.</p>
        </li>
        <li>
          <h3>IP Addresses</h3>
          <p>The IP address that is associated with your search will not be ever saved or shared.</p>
        </li>
        <li>
          <h3>Cookies</h3>
          <p>Cookies is a small piece of data sent from a website and stored in your web browser (computer's hard drive) when you visiting website.</p>
          <p>iZSearch use a small count of cookies that are only necessary for correctly working a webpage GUI widgets.</p>
          <p>You can choose to accept or refuse cookies by changing the settings of your browser. However you should be aware that this action can afect properly functioning of some javascript elements on the webpage.</p>
          <p>Session cookies expire when you close the browser. Persistent such (flash, images etc.) have typical expiration dates ranging from two months up to a couple of years.</p>
        </li>

        <li>
          <h3>Terms</h3>
          <p>This Agreement will enter into force from the moment of your first contact with the iZSearch.com and
            start using any of the services.
          <p>You accept irrevocably and unconditionally and adhere to these Terms as you are accessing, browsing or using the Site and/or any of the Services. </p>
          <p>You also confirm that you agree to be bound by this Agreement without any exemptions, limitations and exclusions, and any and all provisions of this Agreement shall be enforceable to the fullest extent against you. </p>
          <p>You will accept the terms of this Agreement if you access the Site or use any of the Services on behalf a business. In this way iZSearch, Company, their officers, agents, employees and Partners were hold harmless and indemnify from any claim, suit or action arising from or related to the use of the Site and Services or violation of the Agreement, including any liability or expense arising from claims, losses, damages, suits, judgments, litigation costs and attorneys’ fees.</p>
          <p>The User thereby understands and agrees that  iZSearch  owns all copyrights, patent, trademark, know-how and other intellectual property rights (“IPR”) in relation to the Site.</p>
          <p>You should know that the sponsors or advertisers, who provide that content to iZSearch, have intellectual property rights to protect this content  presented to you, as part of the Site or Services. If you haven’t been specifically told by iZSearch or by the owners of that Content, in a separate agreement to modify, rent, lease, loan, sell, distribute or create derivative works based on this content (either in whole or in part), you may not do this.</p>
          <p>The right to review, filter, modify, refuse or remove any or all content from any Service is reserved by iZSearsh. For some of the Services, iZSearch may provide tools to filter out explicit sexual, obscene or indecent content. In addition, the access to material that you may find objectionable, may be limited by the commercially available services and software.</p>
          <p>Some of the Services may display advertisements and promotions, being supported by the advertising revenue. The content of information stored on the Services may be targeted to these advertisements, queries made through the Services or other information.</p>
          <p>The Services may include hyperlinks to other web sites or content or resources. These web sites or resources which are provided by companies or persons other than iZSearch, aren’t controlled by iZSearch.</p>
          <p>If you do not accept the Terms, you may not use the Site and Services.</p>
          <p>iZSrearch may modify these Terms, unilaterally without any notice.</p>
        </li>
      </ul>
    </div>
    <div class="col-md-2"></div>
  </div>
</div>
