<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link type="text/css" rel="stylesheet" href="${baseURL}/resourcescss/keywords/chosen.css"/>
<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/subpages/about/styles/about-us.css"/>
<c:set var="title" value="About" scope="request"/>
<link type="text/css" rel="stylesheet" href="${baseURL}/resources/js/production/plugins/flexislider/flexslider.css"/>
<script type="text/javascript" src="${baseURL}/resources/js/production/plugins/flexislider/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="${baseURL}/resources/js/production/plugins/keywords/chosen.jquery.js"></script>

<jsp:useBean id="fields" class="java.util.ArrayList" scope="request"/>
<jsp:useBean id="words" class="java.util.ArrayList" scope="request"/>
<c:set value="${requestScope.keywords}" var="issues"/>

<div class="container-fluid">
    <div class="header">
        <a class="logo" href="/"><img src="${baseURL}/resources/img/logo.svg" alt="logo"></a>
        <h1>Keywords Manager</h1>
    </div>

    <div class="container">
        <h1 align="center">Always be aware of the latest events in the world, on all your news categories.</h1>
        <div class="art-wrapper">
            <form action="${baseURL}/keywords.html" method="get">
                <input name="q" type="text" placeholder="Check keyword...">
                <button type="su2ed1bmit">CHEK</button>
                <button>${status}</button>
            </form>
            <a href="${baseURL}/keywords_form.html"><button>Buy Keywords</button></a>
            ${offer}
        </div>
    </div>
</div>

<div class="contact">
    <h2>Our Address</h2>

    <dl class="dl-horizontal">
        <dt>Company name</dt>
        <dd>izsearch.com</dd>

        <dt>Address</dt>
        <dd>1921 Palomar Oaks Way, Suite 300</dd>

        <dt>City</dt>
        <dd>Carlsbad</dd>

        <dt>Country</dt>
        <dd>US</dd>

        <dt>State</dt>
        <dd>California</dd>

        <dt>Zip</dt>
        <dd>CA 92008</dd>

        <dt>Phone</dt>
        <dd>(858) 480-9531</dd>

        <dt>Email</dt>
        <dd>business@izsearch.com</dd>
    </dl>
</div>
</div>
<script type="text/javascript">
    var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
        '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        var options = {
            direction: 'horizontal',
            slideshow: true,
            slideshowSpeed: 4000,
            animation: 'fade',
            animationSpeed: 1000,
            animationLoop: true,
            keyboard: false,
            controlNav: false,
            directionNav: false
        };
        $('.flex').flexslider(options);
    });
    </
    style >
