<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz" %>


<c:set var="sliders" value="${requestScope['events']}" scope="request"/>


<%-- Get News --%>
<c:forEach items="${sliders}" var="slider" varStatus="sls">
  <c:set var="status" value=""/>
  <c:if test="${sls.index le 0}">
    <c:set var="status" value="active"/>
  </c:if>
  <c:set var="title" value="${iz:title_normalize(slider.key,1)}"/>
  <div role="tabpanel" class="tab-pane active" data-name="${slider.key}" id="${slider.key}">
    <div id="lfbadge${slider.key}" class="lfbadge lfopen">
      <a href="/${slider.key}.html" data-toggle="tooltip" data-placement="right" title="${title}" data-original-title="${title}">
        <img class="badge_img" src="resources/img/main/slick/${slider.key}.svg">
        <c:choose>
        <c:when test="${slider.key == 'craft'}">
          <span class="category_name" style="display: block;">crafts</span>
        </c:when>
        <c:when test="${slider.key == 'real_estate'}">
          <span class="category_name" style="display: block;">real estate</span>
        </c:when>
        <c:otherwise>
          <span class="category_name" style="display: block;">${slider.key}</span>
          </c:otherwise>
        </c:choose>

        <i class="fa fa-external-link" aria-hidden="true"></i>

      </a>
    </div>
    <section class="grid_layout ${slider.key}">
      <c:if test="${sls.index eq 0}">
        <c:set var="rss" value="${slider.value['rss']}"/>
        <c:if test="${not empty rss}">
          <c:forEach items="${trending}" var="elem" varStatus="esls">

            <c:set var="url" value="${baseURL}/${elem['category']}/${elem['linkId']}/${elem['title4url']}.html"/>
            <c:set var="_img" value="${baseURL}/slides/${elem['cutImage']}"/>
            <c:set var="_fullImg" value="${elem['image']}" />
            <c:choose>
              <c:when test="${elem['imageW'] > elem['imageH']}">
                <c:set var="_imgTipe" value="landscape" />
              </c:when>
              <c:when test="${elem['imageW'] < elem['imageH']}">
                <c:set var="_imgTipe" value="portrait" />
              </c:when>
              <c:otherwise>
                <c:set var="_imgTipe" value="" />
              </c:otherwise>
            </c:choose>
            <c:if test="${empty elem['cutImage'] || elem['cutImage'] eq 'none'}">
              <c:set var="_img" value="${elem['image']}"/>
            </c:if>
            <c:if test="${esls.index == 0}">
              <div class="grid_item first top toggleElem">
                <a href="${url}">
                  <span class="img-cropped ${_imgTipe}">
                      <img src="${elem['image']}">
                  </span>
                  <div class="snuggle">
                    <h6>${elem['title']}</h6>
                    <b class="domain">${elem['strippedUrl']}</b>
                    <b class="date_initialize">${elem['date']}</b>
                    <p class="description">${elem['description']}</p>
                  </div>
                </a>
              </div>
            </c:if>
            <c:if test="${esls.index > 0 && esls.index <= 8}">
                  <c:if test="${esls.index eq 8}">
                    <c:set var="_last" value="last"/>
                  </c:if>
              <c:choose>
                <c:when test="${esls.index < 7}">
                    <div class="grid_item rows top toggleElem">
                </c:when>
                <c:otherwise>
                  <div class="grid_item rows top ${_last}">
                </c:otherwise>
              </c:choose>
                <a href="${url}">
                  <span class="img-cropped ${_imgTipe}"><img src="${elem['image']}"></span>
                  <div class="snuggle">
                    <h6>${elem['title']}</h6>
                    <b class="domain">${elem['strippedUrl']}</b>
                    <b class="date_initialize">${elem['date']}</b>
                    <p class="description">${elem['description']}</p>
                  </div>
                </a>
              </div>
            </c:if>
            <c:if test="${esls.index > 8 && esls.index < 20}">
              <div class="grid_item top">
                <a href="${url}">
                  <div class="img_container">
                    <div class="img_wrap img-cropped ${_imgTipe}">
                      <img src="${_fullImg}">
                    </div>
                  </div>
                  <div class="snuggle">
                    <h6>${elem['title']}</h6>
                    <b class="domain">${elem['strippedUrl']}</b>
                    <b class="date_initialize">${elem['date']}</b>
                    <p class="description">${elem['description']}</p>
                  </div>
                </a>
              </div>
            </c:if>
          </c:forEach>
        </c:if>
        <%--<div class="btnWrp">
          <a href="${slider.key}.html" class="showMoreMain" id="${slider.key}">More ${slider.key}</a>
        </div>--%>
      </c:if>
      <div class="for_news"></div>
    </section>
    <div class="popular-content">
      <section class="popular_cont default_pupular ${slider.key}">
      <c:if test="${sls.index le 0}">
        <c:if test="${not empty rss}">
          <c:set var="idx_couner" value="0" />
          <c:set var="idx_class" value="" />
          <c:forEach items="${popular}" var="el" varStatus="esls">
            <c:set var="ads_couner" value="4" />
            <c:choose>
              <c:when test = "${esls.index eq 4}">
                <c:set var="ads" value="ads 1" />
              </c:when>
              <c:when test = "${esls.index eq 9}">
                <c:set var="ads" value="ads 2" />
              </c:when>
              <c:when test = "${esls.index eq 14}">
                <c:set var="ads" value="ads 3" />
              </c:when>
              <c:otherwise>
                <c:set var="ads" value="" />
              </c:otherwise>
            </c:choose>
            <c:set var="rss" value="${slider_pop.value}"/>
            <c:set var="elem" value="${el['article']}"/>
            <c:set var="url" value="${baseURL}/${elem['category']}/${elem['linkId']}/${elem['title4url']}.html"/>
            <c:set var="wiki" value="${el['wiki']}"/>
            <c:choose>
              <c:when test="${elem['imageW'] > elem['imageH']}">
                <c:set var="_imgTipe" value="landscape" />
              </c:when>
              <c:when test="${elem['imageW'] < elem['imageH']}">
                <c:set var="_imgTipe" value="portrait" />
              </c:when>
              <c:otherwise>
                <c:set var="_imgTipe" value="" />
              </c:otherwise>
            </c:choose>
          <c:if test="${wiki eq true}">
            <div class="item ${ads} wiki">
              <a href="${url}">
                <c:set var="_img" value="${baseURL}/slides/${elem['cutImage']}"/>
                <c:if test="${empty elem['cutImage'] || elem['cutImage'] eq 'none'}">
                  <c:set var="_img" value="${elem['image']}"/>
                </c:if>
                <%--<img src="${_img}" data-lazy="${_img}">--%>
                  <span class="img-cropped ${_imgTipe}"><img src="${elem['image']}"></span>
                  <div class="snuggle">
                    <h6>${el['text']}</h6>
                    <%--<p class="description">${elem['description']}</p>--%>
                  </div>
                </a>
              </div>
            </c:if>

      </c:forEach>
        </c:if>
      </c:if>
      </section>
      <section class="popular_cont more_popular1 ${slider.key}"></section>
      <section class="popular_cont more_popular2 ${slider.key}"></section>
      <section class="popular_cont amazon ${slider.key}"></section>
    </div>
  </div>
</c:forEach>
