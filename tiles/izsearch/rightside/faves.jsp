<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz"%>


<%-- Счетчик для элементов --%>
<c:set var="el_count" value="0" />
<%-- Статус hide/show для элемента --%>
<c:set var="sh_status" value="show" />

<c:forEach var="scored_item" items="${faveslist2draw}" varStatus="el_status">
  <c:set var="item" value="${scored_item.faves}" />
  <c:set var="title" value="${item.title}" />

  <%-- Если элементов больше, чем 15 скрываем их. --%>
  <c:if test="${el_status.index > 14}">
    <c:set var="sh_status" value="hidden" />
  </c:if>
  <li id="${item.id}" data-toggle="bang-popover" data-content="${item.description}" class="<c:out value='${sh_status}'/>">
    <c:set var="img_url" value="/images/faves/${item.id}_${item.title}.png" />
    <c:choose>
      <c:when test="${not empty item.bang}">
        <c:set var="burl" value="${fn:replace(item.bang, 'xxxsearchphrasexxx', q)}" />
        <c:set var="burl" value="${fn:replace(burl, 'XXXsearchphraseXXX', q)}" />
      </c:when>
      <c:when test="${not empty item.directUrl}">
        <c:set var="burl" value="${item.directUrl}" />
      </c:when>
      <c:otherwise>
        <c:set var="burl" value="${item.url}" />
      </c:otherwise>
    </c:choose>
    <a href="${burl}" target="_blank" onclick="_gaq.push(['_trackPageview', '/news/amflinks/mini3/${item.id}']);">
      <img src="${img_url}" />
    </a>
      <%--${item.id} ${item.category.path2root} ${item.category.title} ${scored_item.source} ${scored_item.score}--%>
  </li>
</c:forEach>