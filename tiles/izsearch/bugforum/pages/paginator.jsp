<%--
  User: eFanchik
  Date: 07.07.2014
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz"%>

<c:choose>
  <c:when test="${not empty requestScope.pageHelper}">

    <c:set var="startIndex" value="${requestScope.pageHelper.startIndex}"/>
    <c:set var="endIndex" value="${requestScope.pageHelper.endIndex}"/>
    <c:set var="totalPages" value="${requestScope.pageHelper.totalPages}"/>
    <c:set var="currentIndex" value="${requestScope.pageHelper.currentIndex}"/>
    <c:set var="categoryUrl" value="${requestScope.pageHelper.categoryUrl}"/>
    <c:set var="url" value="${baseUrl}${categoryUrl}"/>
    <c:set var="query" value="${requestScope.q}"/>
    <c:set var="sort" value="${requestScope.sort}"/>
    <c:set var='inactive' value='class=inactive'/>
    <c:set var='first' value='class=first'/>
    <c:set var='last' value='class=last'/>
    <c:set var="next" value='class=next'/>
    <c:set var="previous" value='class=prev'/>

    <c:if test="${endIndex > 0}">
      <nav class="paginator">
        <ul>
            <%--First and Prev urls--%>
          <c:if test="${startIndex > 1}">

            <li <c:out value='${first}'/>>
              <c:url var="firstUrl" value="${url}">
                <c:if test="${not empty query}"><c:param name="q" value="${query}"/></c:if>
              </c:url>
              <a href="<c:out value="${firstUrl}"/>">|&lt;</a>

            <li <c:out value='${previous}'/>>
              <c:url var="prevUrl" value="${url}">
                <c:if test="${not empty query}"><c:param name="q" value="${query}"/></c:if>
                <c:if test="${not empty sort}"><c:param name="sort" value="${sort}"/></c:if>
                <c:param name="page" value="${currentIndex - 10}"/>
              </c:url>
              <a href="<c:out value="${prevUrl}"/>">Prev</a>
            </li>
          </c:if>


            <%--Bounds urls--%>
          <c:forEach
              var="boundaryStart"
              varStatus="status"
              begin="${startIndex}"
              end="${endIndex}"
              step="1">
            <c:choose>
              <c:when test="${status.count > 0 && boundaryStart != currentIndex}">
                <li>
                  <c:url var="currentUrl" value="${url}">
                    <c:if test="${not empty query}"><c:param name="q" value="${query}"/></c:if>
                    <c:if test="${not empty sort}"><c:param name="sort" value="${sort}"/></c:if>
                    <c:param name="page" value="${boundaryStart}"/>
                  </c:url>
                  <a href="<c:out value="${currentUrl}"/>">
                    <c:out value="${boundaryStart}"/>
                  </a>
                </li>
              </c:when>
              <c:otherwise>
                <li>
                  <c:out value="${boundaryStart}"/>
                </li>
              </c:otherwise>
            </c:choose>
          </c:forEach>

            <%--Next and Last urls--%>
          <c:if test="${endIndex<totalPages}">
            <li <c:out value='${next}'/>>
              <c:url var="nextUrl" value="${url}">
                <c:if test="${not empty query}"><c:param name="q" value="${query}"/></c:if>
                <c:if test="${not empty sort}"><c:param name="sort" value="${sort}"/></c:if>
                <c:param name="page" value="${endIndex + 1}"/>
              </c:url>
              <a href="<c:out value="${nextUrl}"/>">Next</a>
            </li>
            <li <c:out value='${last}'/>>
              <c:url var="lastUrl" value="${url}">
                <c:if test="${not empty query}"><c:param name="q" value="${query}"/></c:if>
                <c:if test="${not empty sort}"><c:param name="sort" value="${sort}"/></c:if>
                <c:param name="page" value="${totalPages}"/>
              </c:url>
              <a  href="<c:out value='${lastUrl}'/>">&gt;|</a>
            </li>
          </c:if>
        </ul>
      </nav>
    </c:if>

  </c:when>
  <c:otherwise><c:out value="${paginator}" default="" escapeXml="false"/></c:otherwise>
</c:choose>
