<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Date" %>
<%
  Date now = new Date();
  Calendar cal = Calendar.getInstance();
  cal.setTime(now);

  cal.add(Calendar.DAY_OF_MONTH, -1);
  Date past_day = cal.getTime();

  cal.add(Calendar.DAY_OF_MONTH, -7);
  Date past_week = cal.getTime();

  cal.add(Calendar.MONTH, -1);
  Date past_month = cal.getTime();

  cal.add(Calendar.YEAR, -1);
  Date past_year = cal.getTime();

  cal.add(Calendar.YEAR, -100);
  Date all = cal.getTime();

  SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

  pageContext.setAttribute("now", sdf.format(now));
  pageContext.setAttribute("past_day", sdf.format(past_day));
  pageContext.setAttribute("past_week", sdf.format(past_week));
  pageContext.setAttribute("past_month", sdf.format(past_month));
  pageContext.setAttribute("past_year", sdf.format(past_year));
  pageContext.setAttribute("all", sdf.format(all));
%>

<%--<div id="left_block">
  <ul class="nav navbar-nav">
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-sort-amount-asc"></i>Latest</a>
      <ul class="dropdown-menu">
        <li>
          <form id="latest-form">
            <div class="radio">
              <label><input type="radio" name="in_latest" value="1"> By Time</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="in_latest" value="2" checked> By Relevance</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="in_latest" value="3"> With Images</label>
            </div>
          </form>
        </li>
      </ul>
    </li>
  </ul>
  <ul class="nav navbar-nav">
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-calendar"></i>Date</a>
      <ul id="date-menu-dropdown" class="dropdown-menu">
        <li>
          <div class="fm-popover">
            <form id="specify-date">
              <div class="radio">
                <label><input type="radio" name="time_filter" value="${all},${now}" checked> Any Time</label>
              </div>
              <div class="radio">
                <label><input type="radio" name="time_filter" value="${past_day},${now}"> Past Day</label>
              </div>
              <div class="radio">
                <label><input type="radio" name="time_filter" value="${past_week},${now}"> Past Week</label>
              </div>
              <div class="radio">
                <label><input type="radio" name="time_filter" value="${past_month},${now}"> Past Month</label>
              </div>
              <div class="radio">
                <label><input type="radio" name="time_filter" value="${past_year},${now}"> Past Year</label>
              </div>
              <div class="radio" id="rds-date">
                <label><input type="radio" name="time_filter" value="specify"> Specify</label>
                <label for="date_start">Date from:</label>
                <div class="input-group input-group-sm date" id='dtpicker_from'>
                  <input id="date_start" type="text" class="form-control" name="start" value="" title="Date in format of YYYY/MM/DD (1972/05/21)"/>
                  <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
                <label for="date_end">Date to:</label>
                <div class="input-group input-group-sm date" id='dtpicker_to'>
                  <input id="date_end" type="text" class="form-control" name="end" value="" title="Date in format of YYYY/MM/DD (1972/05/21)"/>
                  <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
              </div>
              <button id="btn-date-apply" type="button" class="btn btn-default" data-event="apply">Apply</button>
            </form>
          </div>
        </li>
      </ul>
    </li>
  </ul>

  <ul class="nav navbar-nav">
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-language" aria-hidden="true"></i>Languages</a>
      <ul class="dropdown-menu">
        <li>
          <div class="fm-popover">
            <form id="languages" action="/" method="get">
              <ul class="language-list">
                <li>
                  <div
                      class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" name="language" id="rdl_all" value="0">All</label>
                  </div>
                </li>
                <li>
                  <div
                      class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" name="language" id="rdl_en" value="en" checked>English
                  </label></div>
                </li>
                <li>
                  <div
                      class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" name="language" id="rdl_de" value="de">Deutsch</label></div>
                </li>
                <li>
                  <div
                      class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" name="language" id="rdl_es" value="es">Español</label></div>
                </li>
                <li>
                  <div
                      class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" name="language" id="rdl_fr" value="fr">Français</label></div>
                </li>
                <li>
                  <div
                      class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" name="language" id="rdl_it" value="it">Italiano</label></div>
                </li>
                <li>
                  <div
                      class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" name="language" id="rdl_ru" value="ru">Русский</label></div>
                </li>
                <li>
                  <div class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" name="language" id="rdl_cn"
                                                                                 value="cn">&#20013;&#25991;&#32;&#40;&#31616;&#20307;&#41;</label>
                  </div>
                </li>
                <li>
                  <div class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" name="language" id="rdl_ja"
                                                                                 value="ja">&#26085;&#26412;&#35486;</label>
                  </div>
                </li>
              </ul>
              <button type="button" id="btn-lang-apply" class="btn btn-default" data-event="save">Apply</button>
            </form>
          </div>
        </li>
      </ul>
    </li>
  </ul>
</div>--%>

<ul id="accordion" class="accordion">
    <li>
        <div class="link" title="Sort by date"><i class="left-icon fa fa-sort-amount-asc"></i><span>Latest</span></div>
        <ul class="submenu">
            <li id="sort-filter">
                <form id="latest-form">
                    <div class="radio">
                        <label><input type="radio" name="in_latest" value="1">By Time</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="in_latest" value="2">By Relevance</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="in_latest" value="3" checked>With Images</label>
                    </div>
                </form>
            </li>
        </ul>
    </li>
    <li>
        <div class="link" title="Search by date"><i class="left-icon fa fa-calendar"></i><span>Date</span></div>
        <ul class="submenu">
            <li>
                <div class="fm-popover">
                    <form id="specify-date">
                        <div class="radio">
                            <label><input type="radio" name="time_filter" value="${all},${now}" checked> Any Time</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="time_filter" value="${past_day},${now}"> Past Day</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="time_filter" value="${past_week},${now}"> Past Week</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="time_filter" value="${past_month},${now}"> Past Month</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="time_filter" value="${past_year},${now}"> Past Year</label>
                        </div>
                        <div class="radio" id="rds-date">
                            <label><input type="radio" name="time_filter" value="specify"> Specify</label>
                            <label for="date_start">Date from:</label>
                            <div class="input-group input-group-sm date" id='dtpicker_from'>
                                <input id="date_start" type="text" class="form-control" name="start" value="" title="Date in format of YYYY/MM/DD (1972/05/21)"/>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            <label for="date_end">Date to:</label>
                            <div class="input-group input-group-sm date" id='dtpicker_to'>
                                <input id="date_end" type="text" class="form-control" name="end" value="" title="Date in format of YYYY/MM/DD (1972/05/21)"/>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>
                        <button id="btn-date-apply" type="button" class="btn btn-default" data-event="apply">Apply</button>
                    </form>
                </div>
            </li>
        </ul>
    </li>
    <li>
      <div class="link" title="Search by languages"><i class="left-icon fa fa-language" aria-hidden="true"></i><span>Language</span></div>
        <ul class="submenu">
            <li>
                <div class="fm-popover">
                    <form id="languages" action="/" method="get">
                        <ul class="language-list">
                            <li>
                                <div
                                        class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" name="language" id="rdl_all" value="0">All</label>
                                </div>
                            </li>
                            <li>
                                <div
                                        class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" name="language" id="rdl_en" value="en" checked>English
                                </label></div>
                            </li>
                            <li>
                                <div
                                        class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" name="language" id="rdl_de" value="de">Deutsch</label></div>
                            </li>
                            <li>
                                <div
                                        class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" name="language" id="rdl_es" value="es">Español</label></div>
                            </li>
                            <li>
                                <div
                                        class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" name="language" id="rdl_fr" value="fr">Français</label></div>
                            </li>
                            <li>
                                <div
                                        class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" name="language" id="rdl_it" value="it">Italiano</label></div>
                            </li>
                            <li>
                                <div
                                        class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" name="language" id="rdl_ru" value="ru">Русский</label></div>
                            </li>
                            <li>
                                <div class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" name="language" id="rdl_cn"
                                                                                               value="cn">&#20013;&#25991;&#32;&#40;&#31616;&#20307;&#41;</label>
                                </div>
                            </li>
                            <li>
                                <div class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" name="language" id="rdl_ja"
                                                                                               value="ja">&#26085;&#26412;&#35486;</label>
                                </div>
                            </li>
                        </ul>
                        <button type="button" id="btn-lang-apply" class="btn btn-default" data-event="save">Apply</button>
                    </form>
                </div>
            </li>
        </ul>
        </li>
    <%--<li>
        <div class="link"><i class="left-icon fa fa-database"></i>Sources<i class="fa fa-chevron-down"></i></div>
        <ul class="submenu">
            <li>
                <div id="source_filter">
                    <c:if test="${not empty domains}">
                        <form id="ds-form">
                            <div class="form-check">
                                <label class="form-check-label"><input class="form-check-input" type="checkbox" name="ds" id="ds_000" value="0" checked>All</label>
                            </div>
                            <c:forEach items="${domains}" var="domain" varStatus="idx">
                                <div class="form-check">
                                    <label class="form-check-label"><input class="form-check-input" type="checkbox" name="ds" id="ds_${domain['id']}" value="${domain['id']}" checked> ${domain['name']}</label>
                                </div>
                            </c:forEach>
                        </form>
                    </c:if>
                </div>
            </li>
        </ul>
    </li>--%>
</ul>

<script type="text/javascript">
    $(document).ready(function () {
        var $body         = $('body');
        var $picker_from  = $('#dtpicker_from');
        var $picker_to    = $('#dtpicker_to');
        $body.on('mouseenter','ul.nav > li.dropdown', function(ev) {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(400);
        });

        $body.on('mouseleave','ul.nav > li.dropdown',function(ev) {
            if($body.find('.datepicker-dropdown').length > 0){
                return false;
            }
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(300);
        });


        $picker_from.datepicker({format: "yyyy/mm/dd", showOnFocus: false, autoclose: true});
        $picker_to.datepicker({format: "yyyy/mm/dd", showOnFocus: false, autoclose: true});

        $picker_from.on("changeDate", function (e) {
            startDate = $(this).datepicker("getDate");
            nowDate = new Date();
            if(startDate > nowDate){
                startDate =  nowDate;
            }
            $(this).datepicker("update", startDate);
            $(this).datepicker("setEndDate", startDate);
            $picker_to.datepicker('setStartDate', startDate);
        });
        $picker_to.on("changeDate", function (e) {
            startDate = $(this).datepicker("getDate");
            nowDate = new Date();
            if(startDate > nowDate){
                startDate =  nowDate;
            }
            $(this).datepicker("update", startDate);
            $(this).datepicker("setEndDate", startDate);
            $picker_from.datepicker('setEndDate', startDate);
        });
        $(function() {
            var Accordion = function(el, multiple) {
                this.el = el || {};
                this.multiple = multiple || false;

                // Variables privadas
                var links = this.el.find('.link');
                // Evento
                links.on('mouseenter', {el: this.el, multiple: this.multiple}, this.dropdown)
            };

            Accordion.prototype.dropdown = function(e) {
                var $el = e.data.el;
                $this = $(this);
                $next = $this.next();

                $next.toggle();
                $this.parent().toggleClass('open');

                if (!e.data.multiple) {
                    $el.find('.submenu').not($next).hide().parent().removeClass('open');
                }
            };

            var accordion = new Accordion($('#accordion'), false);
        });

        // EVENT HANDLERS
        $(document).on('mouseleave', '#accordion li.open', function() {
            $(this).removeAttr('class').children('ul').hide()
        });
        $(document).on('click', '#latest-form', function(){
            console.info('Filter click!!');
        });
    });
</script>
