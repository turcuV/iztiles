<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="iz" uri="/WEB-INF/tlds/izsearch.tld" %>

<!DOCTYPE HTML>
<c:set var="query" value="${empty requestScope.q ? '' : requestScope.q}" scope="request"/>
<c:set var="lang" value="${empty requestScope.locale ? 'en' : fn:substring(requestScope.locale,0, 2)}" scope="request"/>
<c:set var="req" value="${pageContext.request}" scope="request"/>
<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" scope="request"/>
<c:set var="normURL" value="${req.scheme}://${req.serverName}${req.contextPath}" scope="request"/>
<c:set var="fullURL" value="${baseURL}${requestScope['javax.servlet.forward.request_uri']}" scope="request"/>
<c:set var="meta_title" value="${requestScope.meta_title}" scope="request"/>
<c:set var="meta" value="${requestScope.meta}" scope="request"/>
<c:set var="request_uri" value="${requestScope['javax.servlet.forward.request_uri']}"/>
<c:if test="${not empty request_uri}">
    <c:set var="request_uri" value="${fn:replace(request_uri, '/','')}"/>
</c:if>

<html lang="<c:out value="${lang}"/>">
<head>
    <base href="${baseURL}/">
    <meta http-equiv="X-UA-Compatible" content="IE=edge; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:type" content="website" />
    <meta property="og:url" content="${normURL}/${request_uri}?q=${iz:url_encode(param.q)}" />
    <meta property="og:title" content="${meta.title}" />
    <meta property="og:description" content="${meta.descr}" />
    <meta property="og:image" content="${normURL}/images/facebook/${request_uri}.png" />
    <meta property="og:image:width" content="470" />
    <meta property="og:image:height" content="278" />
    <meta property="fb:app_id" content="373533832987702"/>
    <meta name="description" content="${meta.descr}">
    <meta name="keywords" content="${meta.keyword}">
    <title>${meta.title}</title>


    <link rel="shortcut icon" type="image/png" href="${baseURL}/resources/img/favicon.png"/>
    <link rel="search" type="application/opensearchdescription+xml" title="iZSearch" href="${baseURL}/provider.xml">
    <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${baseURL}/resources/font-awesome-4.3.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/izsearch/filters/twoblock/main.css"/>
    <link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/izsearch/filters/twoblock/rss.css"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquerypp.min.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/moment-locales.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/can.min.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/modules/js.cookie.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/modules/imgloaded.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/modules/mry.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/lazyload/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/lazyload/jquery.scrollstop.min.js"></script>
    <script type="text/javascript" src="${baseURL}/resources/js/production/mvc/classes.js"></script>

    <script type="text/javascript">
        var query = "<c:out value='${query}' default=''/>";
        var lang = "<c:out value='${lang}' default='en'/>";
        var geo = {};
    </script>
    <script type="text/javascript" async data-main="${baseURL}/resources/js/production/izsearch/twoblock/main.js" src="${baseURL}/resources/js/production/require.js"></script>
</head>
<body>
<!-- Wrapper -->
<div id="wrapper">
    <div id="related-text"></div>
    <div id="sec01"></div>
    <div id="article-delimeter" class="bistroe-reshenie"></div>
    <div id="sec02"></div>
</div>
</body>
</html>
