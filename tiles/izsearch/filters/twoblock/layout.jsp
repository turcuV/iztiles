<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="iz" uri="/WEB-INF/tlds/izsearch.tld" %>

<!DOCTYPE HTML>
<c:set var="query" value="${empty requestScope.q ? '' : requestScope.q}" scope="request"/>
<c:set var="lang" value="${empty requestScope.locale ? 'en' : fn:substring(requestScope.locale,0, 2)}" scope="request"/>
<c:set var="req" value="${pageContext.request}" scope="request"/>
<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" scope="request"/>
<c:set var="normURL" value="${req.scheme}://${req.serverName}${req.contextPath}" scope="request"/>
<c:set var="fullURL" value="${baseURL}${requestScope['javax.servlet.forward.request_uri']}" scope="request"/>
<c:set var="meta_title" value="${requestScope.meta_title}" scope="request"/>
<c:set var="meta" value="${requestScope.meta}" scope="request"/>
<c:set var="og" value="${requestScope.og}" scope="request"/>
<c:set var="art" value="${requestScope.art}" scope="request"/>
<c:set var="request_uri" value="${requestScope['javax.servlet.forward.request_uri']}"/>
<c:if test="${not empty request_uri}">
	<c:set var="request_uri" value="${fn:replace(request_uri, '/','')}"/>
</c:if>
<c:set var="lside_open" value="${cookie._lside_close.value}" />
<c:if test="${lside_open eq true}">
	<c:set var="lhide" value="no_sidebar"/>
</c:if>

<html lang="<c:out value="${lang}"/>">
	<head>
		<base href="${baseURL}/">
		<meta http-equiv="X-UA-Compatible" content="IE=edge; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta property="og:type" content="website" />
		<meta property="og:url" content="${og['url']}" />
		<meta property="og:title" content="${og['title']}" />
		<meta property="og:description" content="${og['descr']}" />
		<meta property="og:image" content="${og['img']}" />
		<meta property="og:image:width" content="470" />
		<meta property="og:image:height" content="278" />
		<meta property="fb:app_id" content="373533832987702"/>
		<meta name="description" content="${meta.descr}">
		<meta name="keywords" content="${meta.keyword}">
		<title>${meta.title}</title>
		<link rel="shortcut icon" type="image/png" href="${baseURL}/resources/img/favicon.png"/>
		<link rel="search" type="application/opensearchdescription+xml" title="iZSearch" href="${baseURL}/provider.xml">
		<link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap.css"/>
		<link rel="stylesheet" type="text/css" href="${baseURL}/resources/font-awesome-4.3.0/css/font-awesome.min.css"/>
		<link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap-datepicker3.standalone.min.css"/>
		<link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/plugins/slick-1.5.9/slick.css">
		<link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/plugins/share-plugin/share-plugin.css">

		<link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/plugins/justifiedGallery/styles.min.css">
		<link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/izsearch/filters/twoblock/main.css"/>
		<link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/izsearch/filters/twoblock/rss.css"/>
		<link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/plugins/video-player/player.css"/>
		<link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/izsearch/filters/twoblock/modal_view.css"/>
		<%--<link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/plugins/popModal/popModal.css"/>--%>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<link rel="stylesheet" type="text/css" href="/resources/css/ie9_style.css"/>
		<![endif]-->

		<script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquerypp.min.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/moment-locales.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/bootstrap.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/can.min.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/plugins/highlight/pluralize.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/plugins/slick-1.5.9/slick.min.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/plugins/share-plugin/share-plugin.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/lazyload/jquery.lazyload.min.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/lazyload/jquery.scrollstop.min.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/plugins/video-player/player.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/plugins/justifiedGallery/script.min.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/modules/imgloaded.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/modules/mry.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/modules/js.cookie.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/photoZoom.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/mvc/classes.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/izsearch/twoblock/mvc/controls/rss_modal_slider_view.js"></script>
		<%--<script type="text/javascript" src="${baseURL}/resources/js/production/izsearch/twoblock/mvc/controls/rss-filter-tool.js"></script>--%>


		<script type="text/javascript">
			var query = "<c:out value='${query}' default=''/>";
			var lang = "<c:out value='${lang}' default='en'/>";
			var artj = '<%=(String) request.getAttribute("art")%>';
      var geo = {};
		</script>
		<script type="text/javascript" async data-main="${baseURL}/resources/js/production/izsearch/twoblock/main.js" src="${baseURL}/resources/js/production/require.js"></script>

		<!-- Google Analytics counter -->
		<script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-58665066-1', 'auto');
        ga('require', 'linkid', 'linkid.js');
        ga('send', 'pageview');
        setTimeout("ga('send', 'event', 'Pageview', 'Page visit 15 sec. or more')", 1500);
		</script>
	</head>
	<body>
		<!-- Wrapper -->
		<div id="wrapper" class="no_sidebar">
			<div id="rsads-wrapper" class="row"></div>
			<div id="header-wrapper">
				<tiles:insertAttribute name="header"/>
				<tiles:insertAttribute name="carousel" />
			</div>
			<div id="left_sidebar">
				<%--<%@ include file="navigation.jsp" %>--%>
				<tiles:insertAttribute name="navigation"/>
			</div>
			<div id="middle-wrapper">
				<tiles:insertAttribute name="rightblock"/>
			</div>
			<a id="back-to-top" href="#" class="btn btn-lg back-to-top" role="button" ><!--title="Click to return on the top page" data-toggle="tooltip" data-placement="left"--><span class="glyphicon glyphicon-chevron-up"></span></a>
			<span class="socialShare"> <!-- The share buttons will be inserted here --> </span>
		</div>
		<footer>
			<tiles:insertAttribute name="footer"/>
		</footer>

		<!--Back to top button-->
		<script type="text/javascript">
			$(document).ready(function(){

				$(window).scroll(function () {
					if ($(this).scrollTop() > 50) {
						$('#back-to-top').fadeIn();
					} else {
						$('#back-to-top').fadeOut();
					}
				});

				// Scroll body to 0px on click
				$('#back-to-top').click(function () {
					$('#back-to-top').tooltip('hide');
					$('body,html').animate({ scrollTop: 0 }, 800);
					return false;
				});

				$('#back-to-top').tooltip('show');

			});
		</script>
	<%--<div id="pop_Modal"></div>--%>

		<div id="rss_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rss_modal ">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<div class="modal_share trigger-share">
							<ul>
								<li><a class="fb" href="" target="_blank"><i class="fa fa-facebook" ></i></a></li>
								<li><a class="tw" href="" target="_blank"><i class="fa fa-twitter"></i></a></li>
								<li><a class="google" href="" target="_blank"><i class="fa fa-google-plus"></i></a></li>
								<li><a class="pin" href="" target="_blank"><i class="fa fa-pinterest"></i></a></li>
							</ul>
						</div>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<div class="top-content">
              <div class="amazon-slider"></div>
            </div>
            <div class="art-content"></div>
            <div class="similar-content"></div>
						<%--<div id="carousel-modal" class="carousel slide" data-wrap="true" data-interval="false" data-ride="carousel">
							<div class="carousel-inner" role="listbox"></div>

							<a class="left carousel-control" href="#carousel-modal" role="button" data-slide="prev">
								<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control" href="#carousel-modal" role="button" data-slide="next">
								<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>--%>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
