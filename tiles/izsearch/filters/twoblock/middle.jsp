<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/filters/images.css"/>
<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/main_grid.css"/>

<c:set var="fullPath" value="${requestScope['javax.servlet.forward.request_uri']}"/>
<c:set var="domains" value="${requestScope['domains']}"/>
<c:set var="btn_flt_tool" value="${cookie['_dftbtl'].value}" scope="page"/>

<c:choose>
  <c:when test="${btn_flt_tool eq true}">
    <c:set var='visible' value='style="display: block"'/>
  </c:when>
  <c:otherwise>
    <c:set var='visible' value='style="display: none"'/>
  </c:otherwise>
</c:choose>

<div id="search-text">
  <div id="related-title">
    <div id="related-text"></div>
    <div id="also1" class="also"></div>
  </div>
  <div id="sec01" class="sec"></div>
  <div id="article-delimeter">
    <div id="end_search">End of the search results</div>
      <div class="loader" id="loader-2">
        <span></span>
        <span></span>
        <span></span>
      </div>
  </div>
  <div id="also2" class="also">
    <div id="tp_wrapper">
      <div class="wiki-container">
        <div class="wiki-block trending"></div>
        <!--<div class="wiki-block popular"></div>-->
      </div>
    <!--<div id="top">
      <div class="ct"><h2>TOP</h2>
        <div class="info_icon top" data-toggle="popover" data-placement="right" data-content="Top news.">ⓘ</div>
      </div>
      <div class="izslik"></div>
    </div>
    <div class="breaking ct"><h2>LATEST NEWS</h2>
      <div class="info_icon" data-toggle="popover" data-placement="right" data-content="Breaking and most important news of Today.">ⓘ</div>
    </div>
    <div id="breaking">
      <div class="izslik"></div>
    </div>-->
    <div class="title_wrap">
      <div class="trending ct"><h2>Today</h2>
        <!--<div class="info_icon trend" data-toggle="popover" data-placement="right" data-content="">ⓘ</div>-->
      </div>
      <div class="popular ct"><h2>This Week</h2>
        <!--<div class="info_icon pop" data-toggle="popover" data-placement="right" data-content="">ⓘ</div>-->
      </div>
    </div>
    <section class="grid_layout" id="trending"></section>
    <!--<div id="trending">
      <div class="izslik"></div>
    </div>-->
      <div class="popular-content">
        <section id="popular" class="popular_cont"></section>
      </div>

    </div>
  </div>
  <div id="amazon">
    <div class="izslik"></div>
    <div class="ads-title">Ads by Amazon</div>
  </div>
  <div id="adunit0">
    <script type="text/javascript">
      // var c = window.location.href.segment(1) || "";
        var query = window.location.pathname.split('/')[1].split('.')[0];
        console.log(query)
        amzn_assoc_placement = "adunit0";
        amzn_assoc_search_bar = "false";
        amzn_assoc_tracking_id = "izsearchcom0c-20";
        amzn_assoc_search_bar_position = "bottom";
        amzn_assoc_ad_mode = "search";
        amzn_assoc_ad_type = "smart";
        amzn_assoc_marketplace = "amazon";
        amzn_assoc_region = "US";
        amzn_assoc_title = "";
        amzn_assoc_default_search_phrase = query;
        amzn_assoc_default_category = "All";
        amzn_assoc_linkid = "5c74723557905a17a40bd758b812bbe3";
        amzn_assoc_rows = "1";
    </script>
    <script src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US"></script>
  </div>
  <script>
    $(document).ready(function () {
      var query = window.location.href.split('q=');
      if(window.location.href.indexOf("food_recipes") > -1 && !query[1])
      {
        $( "#carousel" ).addClass( "visible" );
      }
      if(window.location.href.indexOf("images") > -1 || window.location.href.indexOf("video") > -1)
      {
        $( ".ct" ).css('display', "none");
      }
      $('.info_icon').popover({
        trigger: 'hover',
        placement: "auto right"
      });

      //Show-Hide trending/popular sliders

      $('body').on('click', '.hide_button', function(){
        $(this).prev().toggleClass('one_slider');
        $(this).html($(this).html() == 'Show More' ? 'Show Less' : 'Show More');
      });

      var url = window.location.pathname;
      var url1 = window.location.search;
      var url2 = url.split('.html');

        if(url1.length && url2[0] !== "/images" && url2[0] !== "/videos") {
            $('#loader-2').show();
        }
    });
  </script>
  <div class="all ct"><h2>ALL</h2>
    <div class="event-motto"></div>
  </div>
    <div id="carousel">
      <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="5000">
        <div class="carousel-inner">
          <%--<div class="item active">
              <blockquote>
                  <div class="event-name">
                      <span>Super-quick <a href="${baseUrl}/food_recipes.html?q=soup">soups</a></span>
                  </div>
              </blockquote>
          </div>--%>
          <div class="item active">
            <blockquote>
              <div class="event-name">
                <span>Healthier <a href="${baseUrl}/food_recipes.html?q=casserole">casseroles</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Use Your <a href="${baseUrl}/food_recipes.html?q=Noodle">Noodle</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Make-ahead <a href="${baseUrl}/food_recipes.html?q=Lunch">lunches</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Ready, <a href="${baseUrl}/food_recipes.html?q=spaghetti">Spaghetti</a>, Go!</span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span><a href="${baseUrl}/food_recipes.html?q=Meatless">Meatless</a> Meals</span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Quick <a href="${baseUrl}/food_recipes.html?q=Chicken">chick recipes</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Quick, high <a href="${baseUrl}/food_recipes.html?q=protein">protein meals</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Easy Spooky <a href="${baseUrl}/food_recipes.html?q=Snacks">Snacks</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>5 Ingredient <a href="${baseUrl}/food_recipes.html?q=Meals">Meals</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span><a href="${baseUrl}/food_recipes.html?q=corn">A-maize-ing</a> recipes</span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Killer <a href="${baseUrl}/food_recipes.html?q=Kale">Kale</a> Dishes</span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Fresh <a href="${baseUrl}/food_recipes.html?q=seafood">Catch</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span><a href="${baseUrl}/food_recipes.html?q=protein">High protein snacks</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span><a href="${baseUrl}/food_recipes.html?q=seasonal">Seasonal Food</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span><a href="${baseUrl}/food_recipes.html?q=spring">Spring</a> meals ideas</span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Veni, <a href="${baseUrl}/food_recipes.html?q=veggie">veggie</a>, vici!</span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Seep your <a href="${baseUrl}/food_recipes.html?q=soup">soup</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Amen to <a href="${baseUrl}/food_recipes.html?q=ramen">ramen</a>!</span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Feeling <a href="${baseUrl}/food_recipes.html?q=blood+orange">bloody</a>?</span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Babushka, balalaika, <a href="${baseUrl}/food_recipes.html?q=pelmeni">pelmeni</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Healthy <a href="${baseUrl}/food_recipes.html?q=avocado">Avocados</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Smoooooooth <a href="${baseUrl}/food_recipes.html?q=smoothie">smoothies</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Going back to the <a href="${baseUrl}/food_recipes.html?q=Root+Vegetables">roots</a></span>
              </div>
            </blockquote>
          </div>
        </div>
      </div>
    </div>
  <div id="sec02" class="sec"></div>
</div>
<div id="filter-images"></div>
