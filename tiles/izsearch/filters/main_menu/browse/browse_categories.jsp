<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<ul class="root-list">
  <c:forEach var="category" items="${categories}">
    <c:set var="children" value="${category.children}"/>
    <li>
      <h2>
        <a href="/browse.html?parentId=${category.id}">${category.title}
      </h2>
      <div class="subtitles">
        <c:forEach var="child" items="${children}" end="3">
              <span>
                <a href="/browse.html?parentId=${child.id}">${child.title}</a>,
              </span>
        </c:forEach>
      </div>
    </li>
  </c:forEach>
</ul>